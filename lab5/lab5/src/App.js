import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";

import rootReducer from './reducers/rootReducer'
import Page from './conteners/Page'


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


function App() {
  return (
    <Provider store={store}>
    <Page></Page>
    </Provider>
  );
}

export default App;
