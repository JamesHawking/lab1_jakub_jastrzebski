const Gpio = require('onoff').Gpio;

pinRed = new Gpio(17, 'out');
pinGreen = new Gpio(27, 'out');
pinSensor = new Gpio(22, 'in', 'both');

pinSensor.watch(function(err, value) {
  if (err) {
    consol.log(err);
  } else {
    setDiodOn(value);
  }

  console.log(value);
});

function setDiodOn(isMove) {
  if (isMove) {
    pinRed.writeSync(0);
    pinGreen.writeSync(1);
  } else {
    pinRed.writeSync(1);
    pinGreen.writeSync(0);
  }
}
